/*
Fecha: 07_01_2021
Autor: fatimaazucenamartinez274@gmail.com
*/

/*Elabora un programa con condicionales anidados que solicite 3 calificaciones,
obtén el promedio de esas tres calificaciones, y de acuerdo al promedio que
se obtuvo, coloca la leyenda que le corresponde, que encontraras en la imagen
que te comparto con el nombre NIVEL DE DESEMPEÑO-TESJI.JPG.
Por ejemplo si tu promedio se encuentra entre 95 y 100 deberá aparecer la
leyenda "EXCELENTE" //El documento se encuentra adjunto*/

#include <stdio.h>

int main(){//Inicio metodo principal
        //Declaracion de variables
        float calificacion_Fisica;
        float calificacion_Programacion;
        float calificacion_Historia;
        float promedio_General;

        printf("Digite la calificacion que obtuvo en Física: ");
        scanf("%f", &calificacion_Fisica);
	printf("Digite la calificacion que obtuvo en Programación: ");
        scanf("%f", &calificacion_Programacion);
        printf("Ingrese la calificacion que obtuvo en Historia: ");
        scanf("%f", &calificacion_Historia);

        promedio_General = (calificacion_Fisica + calificacion_Programacion + calificacion_Historia)/3;

        if ( (promedio_General >= 9.5) && ( promedio_General <= 10.0)){
                        printf("EXCELENTE\n");
        }
                else if (( promedio_General >= 8.5) && ( promedio_General <= 9.44)){
                        printf("NOTABLE\n");
                }
                        else if (( promedio_General >= 7.5) && ( promedio_General <= 8.4)){
                                printf("BUENO\n");
                        }
                                else if (( promedio_General >= 7.0) && ( promedio_General <= 7.4)){
					printf("SUFICIENTE\n");
                                }
                                        else if ( promedio_General < 7.0){
                                                printf("DESEMPEÑO INSUFICIENTE\n");
;
                                        }
}//Fin metodo principal


