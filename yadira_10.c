/*
Fecha: 09_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Calcule la sumatoria de N numeros*/

#include <stdio.h>

int main(){//Inicio método principal
	//Declaración de variables
	int cantidad_Numeros;
	int sumatoria_Numeros;
	int numeros [100];

	printf("¿Cuantos números desea sumar?: ");
	scanf("%d", &cantidad_Numeros);

	for ( int i = 1; i <= cantidad_Numeros; i++){//Inicio for_1
		printf("Digite el numero %d %s", i,": ");
		scanf("%d", &numeros[i]);
		sumatoria_Numeros += numeros[i];
	}//Fin for_1
	printf("La sumatoria de todos los numeros es: %d", sumatoria_Numeros);
}//Fin método principal
