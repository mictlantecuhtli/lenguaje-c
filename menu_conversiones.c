/*
Fecha: 07_04_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
void menu();
void conversion_masa();
void conversion_densidad();
void conversion_presion();
void conversion_volumen();
void conversion_energia();
void conversion_potencia();
void conversion_area();
void conversion_fuerza();
void conversion_velocidad();
void conversion_longitud();
int main(){
	menu();
	return 0;
}
void menu(){
	int opcion;
	printf("BIENVENIDO AL MENU DE OPCIONES");
	printf("\n1. Longitud\n2. Masa\n3. Area\n4. Velovidad\n5. Fuerza");
	printf("\n6. Densida\n7. Presion\n8. Volumen\n9. Energia\n10. Potencia");
	printf("\nDigite su opcion: ");
	scanf("%d", &opcion);

	switch (opcion){
		case 1:
			conversion_longitud();
			return;
		break;
		case 2:
			conversion_masa();
			return;
		break;
		case 3:
			conversion_area();
			return;
		break;
		case 4:
			conversion_velocidad();
			return;
		break;
		case 5:
			conversion_fuerza();
			return;
		break;
		case 6:
			conversion_densidad();
			return;
		break;
		case 7:
			conversion_presion();
			return;
		break;
		case 8:
			conversion_volumen();
			return;
		break;
		case 9:
			conversion_energia();
			return;
		break;
		case 10:
			conversion_potencia();
			return;
		break;
	}
}
void conversion_longitud(){
	int u_entrada, u_salida;
	double valores [] = {1000,100,41.66,3.28,1.09,1,0.001,0.0006215}, resultado, u_valor;
        char arreglo[][50] = {"mm", "cm", "in", "ft", "y", "m","km","mi"};
    	for(int i = 0; i < 8; i++){
        	printf("%d %s \n",i ,arreglo[i]);
    	}
        printf("Digite la unidad de medida de entrada: ");
        scanf("%d", &u_entrada);
        printf("Digite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor numerico de entrada: ");
        scanf("%lf", &u_valor);
	resultado = (valores[u_salida]/valores[u_entrada])*u_valor;
	printf("%lf %s", resultado,"\n");
	return;
}
void conversion_masa(){
	double valores [] = {0.453592,1,453.59,0.031}, resultado, u_valor;
	int u_entrada, u_salida;
	char arreglo [][50] = {"kg","lb","gm","slug"};
        for (int i = 0; i < 4; i++){
		printf("%d %s \n", i, arreglo[i]);
	}
	printf("Digite la unidad de medida de entrada: ");
	scanf("%d", &u_entrada);
        printf("Digite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor de medida de entrada: ");
        scanf("%lf", &u_valor);
        resultado = (valores[u_salida]/valores[u_entrada])*u_valor;
        printf("%lf %s", resultado,"\n");
	return;
}
void conversion_velocidad(){
	int u_entrada, u_salida;
	double u_valor, resultado, valores [] = {1,0.6213,0.91134,0.2777};
	char arreglo [][] = {"km/hr","mi/hr","p/s","m/s"};
	for ( int i = 0; i < 4; i++){
		printf("%d % s \n", i , arreglo[i]);
	}
        printf("Digite la unidad de medida de entrada: ");
        scanf("%d", &u_entrada);
        printf("Digite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor numerico de entrada: ");
        scanf("%lf", &u_valor);
        resultado = (valores[u_salida]/valores[u_entrada])*u_valor;
        printf("%lf \n", resultado);
	return;
}
void conversion_fuerza(){
	int u_salida, u_entrada;
	double u_valor, resultado;
	double valores[] = {1,0.2248,0.101972,100000};
	printf("A elegido la opcion de Fuerza");
        printf("\n\t0)N\n\t1)lbf\n\t2)kgf\n\t3)dinas\nDigite la unidad de medida de entrada: ");
        scanf("%d", &u_entrada);
        printf("\t0)N\n\t1)lbf\n\t2)kgf\n\t3)dinas\nDigite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor de medida de entrada: ");
        scanf("%lf", &u_valor);
        resultado = (valores[u_salida]/valores[u_entrada])*u_valor;
        printf("%lf \n", resultado);
	return;
}
void conversion_area(){
	int u_entrada, u_salida;
	double resultado, u_valor;
	double valores [] = {0.0929,1,0.000087,0.000022957};
	printf("A elegido la opcion de Area");
       	printf("\n\t0)m^2\n\t1)ft^2\n\t2)mi^2\n\t3)acre\nDigite la unidad de medida de entrada: ");
        scanf("%d", &u_entrada);
        printf("\t0)m^2\n\t1)ft^2\n\t2)mi^2\n\t3)acre\nDigite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor numerico de entrada: ");
        scanf("%lf", &u_valor);
        resultado = (valores[u_salida]/valores[u_entrada])*u_valor;
        printf("%lf \n", resultado);
	return;
}
void conversion_densidad(){
	int u_entrada,  u_salida;
	double resultado, u_valor;
	double valores[] = {1,62.428,1000,1.94032};
	printf("A elegido la opcion de Densidad");
        printf("\n\t0)gm/cm^3\n\t1)lb/ft^3\n\t2)kg/m^3\n\t3)slug/ft^3\nDigite la unidad de medida de entrada: ");
        scanf("%d", &u_entrada);
        printf("\t0)gm/cm^3\n\t1)lb/ft^3\n\t2)kg/m^3\n\t3)slug/ft^3\nDigite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor de medida de entrada: ");
        scanf("%lf", &u_valor);
        resultado = (valores[u_salida]/valores[u_entrada])*u_valor;
        printf("%lf \n", resultado);
	return;
}
void conversion_presion(){
        int u_entrada, u_salida;
	double u_valor, resultado;
	double valores[] = {1,0.000145,0.0000098,10,0.02088,0.000750,0.00401};
	printf("A elegido la opcion de Presion");
        printf("\n\t0)N/m^2\n\t1)lbf/pulg^2\n\t2)atm\n\t3)dinas/cm^2\n\t4)lbf/pie^2\n\t5)cmHg");
        printf("\n\t6)pulgada de agua\nDigite la unidad de medida de entrada: ");
        scanf("%d", &u_entrada);
        printf("\t0)N/m^2\n\t1)lbf/pulg^2\n\t2)atm\n\t3)dinas/cm^2\n\t4)lbf/pie^2\n\t5)cmHg");                       
	printf("\n\t6)pulgada de agua\nDigite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor numerico de entrada: ");
        scanf("%lf", &u_valor);
        resultado = (valores[u_salida], valores[u_entrada])*u_valor;
        printf("%lf \n", resultado);
	return;

}
void conversion_volumen(){
	int u_entrada, u_salida;
	double u_valor, resultado;
        double valores[] = {1,0.001,0.0353147,61.0237,1.05696,0.264172,0.219969};
	printf("A elegido la opcion de Volumen");
        printf("\n\t0)litro\n\t1)m^3\n\t2)ft^3\n\t3)pulg^3\n\t4)qt\n\t5)galon americano");
        printf("\n\t6)galon britanico\nDigite la unidad de medida de entrada: ");
        scanf("%d", &u_entrada);
        printf("\t0)litro\n\t1)m^3\n\t2)ft^3\n\t3pulg^3\n\t4)qt\n\t5)galon americano");                              
	printf("\n\t6)galon britanico\nDigite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor numerico de entrada: ");
        scanf("%lf", &u_valor);
        resultado = (valores[u_salida]/valores[u_entrada])*u_valor;
        printf("%lf \n", resultado);
	return;

}
void conversion_energia(){
	int u_entrada, u_salida;
	double u_valor, resultado;
	double valores [] = {4184,4186.8,1,1000,3.96567,3085.96,1.16222,0.00116222,2670.68};
	printf("A elegido la opcion de Energia");
        printf("\n\t0)joule\n\t1)Nm\n\t2)kcal\n\t3)cal\n\t4)Btu");
        printf("\n\t5)lbf/pie\n\t6)watt/hr\n\t7)kw/hr\n\t8)eV\nDigite la unidad de medida de entrada: ");
        scanf("%d", &u_entrada);
        printf("\n\t0)joule\n\t1)Nm\n\t2)kcal\n\t3)cal\n\t4)Btu");
        printf("\t5)lbf/pie\n\t6)watt/hr\n\t7)kw/hr\n\t8)eV\nDigite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor de medida de entrada: ");
        scanf("%lf", &u_valor);
        resultado = (valores[u_salida]/valores[u_entrada])*u_valor;
        printf("%lf", resultado);
	return;
}
void conversion_potencia(){
	int u_entrada, u_salida;
	double u_valor, resultado;
	double valores [] = {745.7,745.7,7456998715.823,8437231,1,0.7457,0.706787};
	printf("A elegido la opcion de Potencia");
        printf("\n\t0)w\n\t1)j/s\n\t2)erg/s\n\t3)cal/h\n\t4)hp\n\t5)kw\n\t6)btu/s");
        printf("\nDigite la unidad de medida de entrada: ");
        scanf("%d", &u_entrada);
        printf("\n\t0)w\n\t1)j/s\n\t2)erg/s\n\t3)cal/h\n\t4)hp\n\t5)kw\n\t6)btu/s");
        printf("\nDigite la unidad de medida de salida: ");
        scanf("%d", &u_salida);
        printf("Digite el valor de medida de entrada: ");
        scanf("%lf", &u_valor);
        resultado = (valores[u_salida]/ valores[u_entrada])*u_valor;
        printf("%lf", resultado);
	return;
}
