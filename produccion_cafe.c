/*
Fecha: 09_06_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
int main(){//Inicio metodo principal
	//Declaracion de variables
	typedef char String [25];
	String nombres [12] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	int i;
	int posicion;
	double total_produccion;
	double suma_produccion [100];

	printf("Digite la produccion de cáfe en KG por meses: \n");
	for (i = 0; i < 12; i++){
		printf("%s %s %s","-->", nombres[i], ": \n");
		scanf("%lf", &suma_produccion[i]);
		total_produccion = total_produccion + suma_produccion[i];
	}

	total_produccion = total_produccion/12;
        printf("Produccion anual: %lf %s", total_produccion,"\n");

	printf("Mayor producción que el promedio: \n");
	for (int i = 0; i < 12; i++){//Inicion for_1
		if (suma_produccion[i] > total_produccion){//Inicio if_1
			printf("--> %lf %s", suma_produccion[i],"\n");
		}
	}//Fin for_1
	
	printf("Menor produccion de cáfe que el promedio: \n");
	for (int i = 0; i < 12; i++){//Inicion for_1
                if (suma_produccion[i] < total_produccion){//Inicio if_1
                        printf("--> %lf %s", suma_produccion[i],"\n");
		}
        }//Fin for_1

	for (int x = 0; x < 12; x++){//Inicio for_2
            for (int y = 0; y < 11; y++){//Inicio for_2
                if (suma_produccion[y] > suma_produccion[y+1]){//Inicio condicional if
                    posicion = suma_produccion[y + 1];
                    suma_produccion[y+1] = suma_produccion[y];
                    suma_produccion[y] = posicion;
                }//Fin condicional if
            }//Fin for_3
        }//Fin for_1
	
	printf("La mayor produccion de cáfe es de: %lf %s",suma_produccion[11], " kg");
	printf("\nLa menor produccion de cáfe es de: %lf %s", suma_produccion[0]," kg \n");
}//Fin metodo principal
