/*
 *Fecha: 03_10_2020
 *Autor: Fatima Azucena Martinez Cadena. 
 *fatimaazucenamartinez274@gmail.com*/

/*Calcula la cantidad de euros a monedas*/

#include <stdio.h>

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
        double EURO = 0.039783;
        double conver = 0;
        int opcion;
        int continuar;
        int pesoMexicano;
        double DOLLAR = 22.91;

        do{
            printf("*Bienvenido al menú de conversiones*\n1)Dolares \n2)Euros");
            printf("\nDigite su opcion: ");
            scanf("%d", &opcion);

            switch (opcion){/*Inicio switch case*/
                  case 1:
                        printf("Usted a elegido una conversion de pesos mexicanos a dolares");
                        printf("\nIngrese la cantidad de dinero que desea convertir: ");
                        scanf("%d", &pesoMexicano);
                        conver = (pesoMexicano/DOLLAR);
                        printf("Usted tiene: $ %f %s",conver,"dolares");
                  break;
                  case 2:
                        printf("Usted a elegido la conversion de pesos mexicanos a euros");
                        printf("\nIngrese la cantidad de dinero que desea convertir: ");
                        scanf("%d", &pesoMexicano);
                        conver = (pesoMexicano/EURO);
			                        printf("Usted tiene: € %f %s",conver,"euros");
                  break;
                  default:
                        printf("Opcion no valida");
            }/*Fin switch case*/

        printf("\n¿Que desea hacer?\n1)VOLVER\n0)SALIR");
        printf("\nDigite su opcion: ");
        scanf("%d", &continuar);
      }while(continuar==1);
}/*Fin metodo principal*/
