/*
Fecha: 08_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmial.com
*/

/*EN UNA TIENDA DEPARTAMENTAL ESTA PUBLICANDO NUEVAS OFERTAS POR
TEMPORADA NAVIDEÑA. SI LOS CLIENTES PRESENTAN UNA TARJETA DE CLIENTE FRECUENTE.
LES OTORGARÁN UN DESCUENTO DEL 20% DEL TOTAL DE SU COMPRA.
*/

#include <stdio.h>

int main(){//Inicio metodo principal
        //Declaracion de variables
        int opcion;
        int continuar;
        int numVentas;
        float precio_Venta = 0;
        float venta_Descuento [100];
        float venta_no_Descuento [100];

        do {
                printf("B I E N V E N I D O S\n\t¿Cuenta con tarjeta de cliente frecuente?: \n\t1)Si\n\t2)No");
                printf("\n\tDigite su opcion: ");
                scanf("%d", &opcion);

                switch (opcion){
                        case 1:
                                printf("Usted cuenta con tarjeta de cliente frecuente");
                                printf("\nDigite el numero de ventas que realizo: ");
                                scanf("%d", &numVentas);

                                for ( int i = 1; i <= numVentas ; i++){
                                        printf("Inserte el precio de la venta %d %s ", i , ": $");
                                        scanf("%f", &venta_Descuento[i]);

                                        precio_Venta += venta_Descuento[i];
                                }
                                precio_Venta = precio_Venta - (precio_Venta*0.20);
                                printf("El monto total es de: $ %f %s", precio_Venta , " pesos");
                        break;
                        case 2:
                                precio_Venta = 0;
                                printf("Usted no cuenta con tarjeta de cliente frecuente");
                                printf("\nDigite el numero de ventas que realizo: ");
                                scanf("%d", &numVentas);

                                for ( int i = 1; i <= numVentas ; i++){
                                        printf("Inserte el precio de la venta %d %s", i , ": $");
                                        scanf("%f", &venta_no_Descuento[i]);

                                        precio_Venta += venta_no_Descuento[i];
                                }
                                precio_Venta = precio_Venta - (precio_Venta*0.20);
                                printf("El monto total es de: $ %f %s", precio_Venta , " pesos");

                        break;
                        default:
                                printf("La opcion no es válida, intente de nuevo");
                        break;
                }
                printf("\n¿Que desea hacer?\n\t1)Volver al ménu \n\t0)Salir");
                printf("\n\tDigite su opcion: ");
                scanf("%d", &continuar);

        }while (continuar == 1);
}//Fin metodo principal


