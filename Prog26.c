/*
 *Fecha: 29_09_20
 *Autor: Fátima Azucena Martínez Cadena
 <fatimaazucenamartinez274@gmail.com>
 */

/*26-El banco “Bandido de peluche” desea calcular para
cada uno de sus N clientes su saldo actual, su pago mínimo 
y su pago para no generar intereses. Además, quiere calcular 
el monto de lo que ganó por con­cepto interés con los clientes morosos. 
Los datos que se conocen de cada cliente son: saldo anterior, monto de 
las compras que realizó y pago que depositó en el corte anterior. 
Para calcular el pago mínimo se considera 15% del saldo actual, y
el pago para no generar intere­ses corresponde a 85% del saldo actual,
considerando que el saldo actual debe incluir 12% de los intereses
causados por no realizar el pago mínimo y $200 de multa por el mismo motivo.
*/

#include <stdio.h>

int main(){/*Inicio metodo principal*/
	/*Declaración de variables*/
	float saldoAnterior = 0;
	float pagoActual = 0;
	float saldoActual = 0;
	float saldoMinimo = 0;
	float pagoInteres = 0;
	float montoCompras = 0;
	float depositoAnterior = 0;
	int numClientes;
	char nombre[20];

	printf("Ingrese la cantidad de cliente: ");
	scanf("%d", &numClientes);

	for(int i=0;i<=numClientes;i++){/*Inicio for_1*/
		printf("Ingrese el nombre del cliente  %d %s",i,": " );
		scanf("%s", &nombre);
		printf("Ingrese el saldo anterior: ");
		scanf("%f", &saldoAnterior);
		printf("Ingrese su ultimo deposito: ");
		scanf("%f", &depositoAnterior);
		printf("Ingrese el compro por sus ventas realizadas: ");
		scanf("%f", &montoCompras);
		printf("Ingrese el saldo actual: ");
		scanf("%f", &saldoActual);

		pagoActual = (saldoActual*0.12)+200;
		saldoMinimo = (saldoActual*0.15);
		pagoInteres = (saldoActual*0.85);

		printf("El saldo actual de: %s %s %f %s",nombre," es de: $",pagoActual," pesos \n");
		printf("El saldo minimo de: %s %s %f %s",nombre," es de: $",saldoMinimo," pesos \n");
		printf("El saldo actual de: %s %s %f %s",pagoInteres," es de: $"," pesos \n");
	}/*Fin for_1*/
}/*Fin metodo principal*/
