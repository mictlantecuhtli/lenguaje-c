/*
Fecha: 28_12_2020
Autor: Fatima Azucena MC
Correo: fatimaazucanamartinez274@gmail.com
*/

/*Obtener la edad de una persona en meses, si se ingresa su edad
en años y meses. Ejemplo: Ingresado 3 años 4 meses debe
mostrar 40 meses.*/

#include <stdio.h>

int main(){//Inicio metodo principal
	//Declaracion devariables
	int edad;
	int anyos;
	int meses;

	printf("Ingrese el numero de años que tiene: ");
	scanf("%d", &anyos);
	printf("Con cuantos meses: ");
	scanf("%d", &meses);

	edad = edad + (anyos*12) + meses;

	printf("Su edad en meses es de: %d", edad);
}//Fin metodo principal 
