/*
Fecha: 24_11_2020
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Escriba un algoritmo que, dado el número de horas trabajadas por un empleado 
y el sueldo por hora, calcule el sueldo total de ese empleado. */

//Libreria principal
#include <stdio.h>

int main(){//Inicio metodo principal
	//Declaracion de variables
	String nombre;
	int hTrabajadas;
	double precioHora;
	double sueldoTotal;

	printf("Ingrese el nombre del empleado: ");
	scanf("%d", &nombre);
	printf("Ingrese el precio por hora: ");
	scanf("%f", &precioHora);
	sueldoTotal = precioHora*hTrabajadas;

	printf("El sueldo de %s %s %f %s", nombre ," es de: ", sueldoTotal, "peso");

}//Fin metodo principal
