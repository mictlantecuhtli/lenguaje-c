/*Fecha: 02_10_2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Una persona recibe un préstamo de $10,000.00 de un
banco y desea saber cuánto pagará de interés, si el 
banco le cobfgra una tasa del 27% anual.*/

#include <stdio.h>
int main(){/*Inicio metodo principal*/
        /*Declaración de variables*/
        int tiempo;
        float PRESTAMO = 10000;
        int tiempoTranscurrido;
        float INTERES = 0.27;
        int i;

        printf("Ingrese el año en que saco el prestamo: ");
        scanf("%d", &tiempo);
        printf("Ingrese el año actual: ");
        scanf("%d", &tiempoTranscurrido);

        for(i = tiempo;i<=tiempoTranscurrido;i++){/*Inicio for_1*/
                PRESTAMO = PRESTAMO+(PRESTAMO*INTERES);

                printf("El interes del año %d %s %f %s",i," es de: $",PRESTAMO," pesos \n");
        }/*Fin for */
}/*Fin metodo principal*/
