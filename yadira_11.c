/*
Fecha: 10_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Calcular la tabla de multiplicar de cualquier
numero*/

#include <stdio.h>

int main(){//Inicio método principal
	//Declaracion de variables
	int numero;
	int tabla;
	int resultado;

	printf("¿Que tabla desea?: ");
	scanf("%d", &tabla);
	printf("¿Hasta que numero desea ver la tabla?: ");
	scanf("%d", &numero);

	for ( int i = 1; i < numero; i++){
		resultado = tabla * i;
		scanf("%d %s %d %s %d", tabla ,"x", numero,"=", resultado);
	}
}//Fin método principal
