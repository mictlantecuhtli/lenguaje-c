/*
Fecha: 7_04_2021
Autor: Fátima Azucena MC
Correo: fatimaazucenamartinez274@gmial.com
*/
#include <stdio.h>
	
typedef char String[25];
void desprender_menu(int valor);
String nombres[10][8] = {
	{"mm","cm","in","ft","y","m","km","mi"},
	{"m^2","ft^2","mi^2","acre","No válido","No válido","No válido","No válido"},
	{"litro","m^3","ft^3","galon americano","galon britanico","No válido","No válido","No válido"},
	{"kg","lb","gm","slug","No válido","No válido","No válido","No válido"},
	{"km/hr","mi/hr","p/s","m/s","No válido","No válido","No válido","No válido"},
	{"gm/cm^3","lb/ft^3","kg/m^3","slug/ft^3","No válido","No válido","No válido","No válido"},
	{"N","lbf","kgf","dinas","No válido","No válido","No válido","No válido"},
	{"J","Nm","cal","Btu","lbf/ft","w/hr","kw/hr","eV"},
	{"w","j/s","erg/s","cal/h","hp","kw","btu/s","No válido"},
	{"N/m^2","lbf/in^2","atm","d/cm^2","lbf/ft^2","pulgada de agua","No válido","No válido"},
};
double valores[10][8] = {
	{1000,100,41.66,3.28083,1.09,0.001,0.0006215},
	{0.0929,1,0.000087,0.000022957},
	{1,0.001,0.0353147,61.0237,1.05696,0.264172,0.219969},
	{0.453592,1,453.59,0.031},
	{1,0.6213,0.91134,0.2777},
	{1,62.428,1000,1.94032},
	{1,0.2248,0.101972,100000},
	{4184,1,1000,3.96567,3085.96,1.16222,0.00116222,2670.68},
	{745.7,745.7,7456998715.823,8437231,1,0.7457,0.706787},
	{1,0.000145,0.0000098,10,0.02088,0.000750,0.00401}
};
String temperatura [3] = {"C°","F°","K°"};
int main(){//Inicio metodo principal
	int entrada;
	int salida;
	double valor_entrada;
	double resultado;
	String menu2[11] = {"Longitud","Area","Volumen","Masa","Velocidad","Densidad","Fuerza","Energia","Potencia","Presion","Temperatura"};
	int opcion;
	for( int i = 0; i < 11; i++){
		printf("%d %s \n", i, menu2[i]);
	}
	printf("Digite su opcion: ");
	scanf("%d", &opcion);

	if (opcion == 10){
		 for (int i = 0; i < 3; i++){    
                 	printf("%d %s \n", i , temperatura[i]);
		}
		
		printf("Digite la unidad de entrada: ");
		scanf("%d", &entrada);
		printf("Digite la unidad de salida: ");
		scanf("%d", &salida);
		printf("Digite el valor numerico de entrada: ");
		scanf("%lf", &valor_entrada);

		if ( (entrada == 0)&&(salida == 1)){
			resultado = (valor_entrada*1.8)+32;
			printf("%lf %s", resultado,"\n");
		}
		else if ((entrada == 0)&&(salida == 2)){
			resultado = valor_entrada + 273.15;
			printf("%lf %s", resultado,"\n");
		}
		else if ( (entrada == 1)&&(salida == 0)){
			resultado = (valor_entrada-32)*0.555555556;
			printf("%lf %s", resultado,"\n");
		}
		else if ((entrada == 1)&&(salida == 2)){
			resultado = (valor_entrada-32)*0.555555556+273.15;
			printf("%lf %s", resultado,"\n");
		}
		else if ((entrada == 2)&&(salida == 0)){
			resultado = valor_entrada-273.15;
			printf("%lf %s", resultado,"\n");
		}
		else if ((entrada == 2)&&(salida == 1)){
			resultado = (valor_entrada-273.15)*1.8+32;
			printf("%lf %s", resultado,"\n");
		}
	}
	else {
		desprender_menu(opcion);
 	        printf("Unidad de entrada: ");
 	        scanf("%d", &entrada);
 	        printf("Unidad de salida: ");
		scanf("%d", &salida);
 
 	        printf("Digite el valor numerico de entrada: ");
		scanf("%lf", &valor_entrada);
 
 		resultado = (valores[opcion][salida] / valores[opcion][entrada])*valor_entrada;
 		printf("%lf %s", resultado, "\n");
	}
}//Fin metodo principal
void desprender_menu(int valor){
	for (int i = 0; i < 8; i++){
		printf("%d %s \n", i , nombres[valor][i]);
	}
}
