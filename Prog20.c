/*Fecha: 19-08-2020
 * Autor:Fatima Azucena MC
 * fatimaazucenamartinez274@gmail.com */

/*Se requiere determinar el tiempo que tarda una persona en llegar de una ciudad a otra en bicicleta, considerando que lleva una velocidad constante.*/

/*Libreria principal*/
#include <stdio.h>

int main(){/*Inicio metodo principal*/
	/*Declaracion de variables*/
	float tiempo;
	float distancia;
	float velocidad;

	printf("Ingrese la distancia recorrida: ");
	scanf("%f", &distancia);
	printf("Ingrese la velocidad en la que manejaba: ");
	scanf("%f", &velocidad);

	tiempo=(distancia/velocidad);

	printf("El tiempo que tarda en llegar la persona es de: %f %s", tiempo, " hrs");

}/*Fin metodo principal*/

