#include <koolplot.h>
#include <stdio.h>

void graph(double g);

int main()
{
	 double a = 5.5, b = 2.5, gh;
	 
	 gh = a / b;
     
	 graph(gh);	

         return 0;
}



void graph(double g)
{
	plotdata x(-10, 10);
	plotdata y = g;
	plot(x,y);
}
