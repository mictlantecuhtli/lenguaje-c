/*
Fecha: 06_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Genera un programa que determine si eres de mayor de edad*/

//Libreria principal
#include <stdio.h>

int main(){//Inicio método principal
        //Declaracion de variables
        int edad;

        printf("Digite su edad: ");
        scanf("%d", &edad);

                if ( edad >=18){//Inicio condicional if_1
                        printf("Usted es mayor de edad :D");
                }
                else{
                        printf("Usted no es mayor de edad :(");

