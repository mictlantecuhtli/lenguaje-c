/*
Fecha: 16_06_2021
Autor: Fátima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int algoritmoDDA(int x1, int y1, int x2, int y2);

int main(){
	int a1;
	int b1;
	int a2;
	int b2;

	printf("Ingrese el valor de la cordenada x1: ");
	scanf("%d", &a1);
	printf("Ingrese el valor de la cordenada y1: ");
        scanf("%d", &b1);
	printf("Ingrese el valor de la cordenada x2: ");
        scanf("%d", &a2);
	printf("Ingrese el valor de la cordenada y2: ");
        scanf("%d", &b2);

	algoritmoDDA(a1,b1,a2,b2);
}
int algoritmoDDA(int x1, int y1, int x2, int y2){
	int dx;
	int dy;
	int steps;
	int xinc;
	int yinc;

	dx = abs(x2-x1);
	dy = abs(y2-y1);

	if ( dx > dy){
		steps = dx;	
	}
	else {
		steps = dy;
	}

	xinc = dx / steps;
	yinc = dy / steps;

	for (int i = 0; i <= steps; i++){
		printf("%s %d %s %d %s", "x: ", x1, " y: ",y1,"\n");
		x1 = x1 + xinc;
		y1 = y1 + yinc;
	}
}
