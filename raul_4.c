/*
 *Fecha: 07_10_2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Calcular el descuento y el monto a pagar por un medicamento
cualquiera en una farmacia si todos los medicamentos tienen un 
descuento del 35%.*/

#include <stdio.h>

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
        float DES = 0.35;
        char medicamento[20];
        float precio;

        printf("Ingrese el nombre de su medicamento: ");
        scanf("%s", &medicamento);
        printf("Ingrese el precio del medicamento: ");
        scanf("%f", &precio);

        precio = precio-(precio*DES);

        printf("El total a pagar por su  %s %s %f %s  ",medicamento," es de: $", precio ," pesos");

}/*Fin metodo principal*/
