/*
Fecha: 13_03_2021
Autor: Fatima MC
Correo: fatimaazucenamartinez274@gmail.com 
*/
#include <stdio.h>
double conversiones_potencia (double unidad_entrada_i, double unidad_salida_i, double valor_i);
double conversiones_energia (double unidad_entrada_e, double unidad_salida_e, double valor_e);
double conversiones_volumen (double unidad_entrada_n, double unidad_salida_n, double valor_n);
double conversiones_fuerza (double unidad_entrada_f, double unidad_salida_f, double valor_f);
double conversiones_densidad (double unidad_entrada_d, double unidad_salida_d, double valor_d);
double conversiones_presion (double unidad_entrada_p, double unidad_salida_p, double valor_p);
double conversiones_velocidad (double unidad_entrada_v, double unidad_salida_v, double valor_v);
double conversiones_masa (double unidad_entrada_m, double unidad_salida_m, double valor_m);
double conversiones_longitud (double unidad_entrada_l, double unidad_salida_l, double valor_l);
double conversiones_area (double unidad_entrada_a, double unidad_salida_a, double valor_a);
int main(){//Inicio metodo principal
	//Declaracion de varibales
	int opcion;
	int continuar;
	int u_entrada;
	int u_salida;
	double u_valor;
	double valor_conversion;
	double valores_potencia [] = {745.7,745.7,7456998715.823,8437231,1,0.7457,0.706787};
	double valores_energia [] = {4184,4186.8,1,1000,3.96567,3085.96,1.16222,0.00116222,2670.68};
	double valores_volumen [] = {1,0.001,0.0353147,61.0237,1.05696,0.264172,0.219969};
	double valores_fuerza [] = {1,0.2248,0.101972,100000};
	double valores_densidad [] = {1,62.428,1000,1.94032};
	double valores_presion [] = {1,0.000145,0.0000098,10,0.02088,0.000750,0.00401};
	double valores_masa [] = {0.453592,1,453.59,0.031};
	double valores_velocidad [] = {1,0.6213,0.91134,0.2777};
	double valores_longitud [] = {1000,100,41.66,3.28,1.09,1,0.001,0.0006215};
	double valores_area [] = {0.0929,1,0.000087,0.000022957};
	do{
		printf("Bienvenido al menu de conversiones");
		printf("\n\t1)Longitud\n\t2)Area\n\t3)Masa\n\t4)Velocidad");
		printf("\n\t5)Presion\n\t6)Volumen\n\t7)Fuerza\n\t8)Dendidad");
		printf("\n\t9)Energia\n\t10)Potencia\nDigite su opcion: ");
		scanf("%d", &opcion);

		switch (opcion){//Inicio switch-case_1
			case 1:
        			printf("A elegido Longitud");
          			printf("\n0)mm\n1)cm\n3)pl\n4)ft\n5)y\n6)m\n7)km\n8)\nmiDigite la unidad de medida de entrada: ");
         			scanf("%d", &u_entrada);
         			printf("Digite la unidad de medida de salida: ");
        			scanf("%d", &u_salida);
         			printf("Digite el valor numerico de entrada: ");
 	        		scanf("%lf", &u_valor);
				valor_conversion = conversiones_longitud(valores_longitud [u_entrada], valores_longitud [u_salida], u_valor);
				printf("%lf", valor_conversion);
			break;
			case 2:
				printf("A elegido la opcion de Area");
				printf("\n\t0)m^2\n\t1)ft^2\n\t2)mi^2\n\t3)acre\nDigite la unidad de medida de entrada: ");
				scanf("%d", &u_entrada);
				printf("Digite la unidad de medida de salida: ");
				scanf("%d", &u_salida);
				printf("Digite el valor numerico de entrada: ");
				scanf("%lf", &u_valor);
				valor_conversion = conversiones_area(valores_area [u_entrada], valores_area [u_salida], u_valor);
				printf("%lf", valor_conversion);
			break;
			case 3:
				printf("A elegido la opcion de Masa");
				printf("\n\t0)kg\n\t1)lb\n\t2)gm\n\t3)slug\nDigite la unidad de medida de entrada: ");
				scanf("%d", &u_entrada);
				printf("Digite la unidad de medida de salida: ");
                                scanf("%d", &u_salida);
				printf("Digite el valor de medida de entrada: ");
				scanf("%lf", &u_valor);
				valor_conversion = conversiones_masa (valores_masa[u_entrada], valores_masa[u_salida], u_valor);
				printf("%lf", valor_conversion);
			break;
			case 4:
				printf("A elegido la opcion de Velovidad");
				printf("\n\t0)km/hr\n\t1)mi/hr\n\t2)p/s\n\t3)m/s\nDigite la unidad de medida de entrada: ");
				scanf("%d", &u_entrada);
				printf("Digite la unidad de medida de salida: ");
                                scanf("%d", &u_salida);
				printf("Digite el valor numerico de entrada: ");
				scanf("%lf", &u_valor);
				valor_conversion = conversiones_velocidad (valores_velocidad[u_entrada], valores_velocidad[u_salida], u_valor);
				printf("%lf", valor_conversion);
			break;
			case 5:
				printf("A elegido la opcion de Presion");
				printf("\n\t0)N/m^2\n\t1)lbf/pulg^2\n\t2)atm\n\t3)dinas/cm^2\n\t4)lbf/pie^2\n\t5)cmHg");
				printf("\n\t6)pulgada de agua\nDigite la unidad de medida de entrada: ");
				scanf("%d", &u_entrada);
				printf("Digite la unidad de medida de salida: ");
                                scanf("%d", &u_salida);
				printf("Digite el valor numerico de entrada: ");
                                scanf("%lf", &u_valor);
				valor_conversion = conversiones_presion (valores_presion[u_entrada], valores_presion[u_salida], u_valor);
				printf("%lf", valor_conversion);
			break;
			case 6:
				printf("A elegido la opcion de Volumen");
                                printf("\n\t0)litro\n\t1)m^3\n\t2)ft^3\n\t3)pulg^3\n\t4)qt\n\t5)galon americano");
				printf("\n\t6)galon britanico\nDigite la unidad de medida de entrada: ");
				scanf("%d", &u_entrada);
                                printf("Digite la unidad de medida de salida: ");
                                scanf("%d", &u_salida);
                                printf("Digite el valor numerico de entrada: ");
                                scanf("%lf", &u_valor);
                                valor_conversion = conversiones_volumen (valores_volumen[u_entrada], valores_volumen[u_salida], u_valor);
                                printf("%lf", valor_conversion);
			break;
			case 7:
				printf("A elegido la opcion de Fuerza");
                                printf("\n\t0)N\n\t1)lbf\n\t2)kgf\n\t3)dinas\nDigite la unidad de medida de entrada: ");
                                scanf("%d", &u_entrada);
                                printf("Digite la unidad de medida de salida: ");
                                scanf("%d", &u_salida);
                                printf("Digite el valor de medida de entrada: ");
                                scanf("%lf", &u_valor);
                                valor_conversion = conversiones_fuerza (valores_fuerza[u_entrada], valores_fuerza[u_salida], u_valor);
                                printf("%lf", valor_conversion);
			break;
			case 8:
				printf("A elegido la opcion de Densidad");
                                printf("\n\t0)gm/cm^3\n\t1)lb/ft^3\n\t2)kg/m^3\n\t3)slug/ft^3\nDigite la unidad de medida de entrada: ");
				scanf("%d", &u_entrada);
				printf("Digite la unidad de medida de salida: ");
                                scanf("%d", &u_salida);
                                printf("Digite el valor de medida de entrada: ");
                                scanf("%lf", &u_valor);
                                valor_conversion = conversiones_densidad (valores_densidad[u_entrada], valores_densidad[u_salida], u_valor);
                                printf("%lf", valor_conversion);
			break;
			case 9:
				printf("A elegido la opcion de Energia");
                                printf("\n\t0)joule\n\t1)Nm\n\t2)kcal\n\t3)cal\n\t4)Btu");
                                printf("\n\t5)lbf/pie\n\t6)watt/hr\n\t7)kw/hr\n\t8)eV\nDigite la unidad de medida de entrada: ");
				scanf("%d", &u_entrada);
				printf("Digite la unidad de medida de salida: ");
                                scanf("%d", &u_salida);
                                printf("Digite el valor de medida de entrada: ");
                                scanf("%lf", &u_valor);
                                valor_conversion = conversiones_energia (valores_energia[u_entrada], valores_energia[u_salida], u_valor);
                                printf("%lf", valor_conversion);
			break;
			case 10:
				printf("A elegido la opcion de Potencia");
 				printf("\n\t0)w\n\t1)j/s\n\t2)erg/s\n\t3)cal/h\n\t4)hp\n\t5)kw\n\t6)btu/s");
                                printf("\nDigite la unidad de medida de entrada: ");
                                scanf("%d", &u_entrada);
                                printf("Digite la unidad de medida de salida: ");
                                scanf("%d", &u_salida);
                                printf("Digite el valor de medida de entrada: ");
                                scanf("%lf", &u_valor);
                                valor_conversion = conversiones_potencia (valores_potencia[u_entrada], valores_potencia[u_salida], u_valor);
                                printf("%lf", valor_conversion);
			break;
		}//Fin switch-case_1
		printf("\n¿Que desea hacer?\n1)Volver al menu\n0)Salir");
		printf("\nDigite su opcion: ");
		scanf("%d", &continuar);
	}while ( continuar == 1);
	printf("Hasta la proxima\n");
}//Fin metodo principal 
double conversiones_longitud (double unidad_entrada_l, double unidad_salida_l, double valor_l){//Inicio funcion conversiones longitud
        double resultado;        
	return resultado = (unidad_salida_l/unidad_entrada_l)*valor_l;
}//Fin funcion conversiones longitud
double conversiones_area (double unidad_entrada_a, double unidad_salida_a, double valor_a){//Inicio funcion conversiones area
	double resultado_a;
	return resultado_a = (unidad_salida_a/unidad_entrada_a)*valor_a;
}//Fin funcion conversiones area
double conversiones_masa (double unidad_entrada_m, double unidad_salida_m, double valor_m){//Inicio funcion conversion masa
	double resultado_m;
	return resultado_m = (unidad_salida_m/unidad_entrada_m)*valor_m;
}//Fin funcion conversion masa
double conversiones_velocidad (double unidad_entrada_v, double unidad_salida_v, double valor_v){//Inicio funcion conversion velocidad
	double resultado_v;
	return resultado_v = (unidad_salida_v/unidad_entrada_v)*valor_v;
}//Fin funcion conversion velocidad
double conversiones_presion (double unidad_entrada_p, double unidad_salida_p, double valor_p){//Inicio funcion conversion presion
	double resultado_p;
	return resultado_p = (unidad_salida_p/unidad_entrada_p)*valor_p;
}//Fin funcion conversion presion
double conversiones_volumen (double unidad_entrada_n, double unidad_salida_n, double valor_n){//Inicio funcion conversion volumen
	double resultado_n;
	return resultado_n = (unidad_salida_n/unidad_entrada_n)*valor_n;
}//Fin funcion conversion volumen
double conversiones_fuerza (double unidad_entrada_f, double unidad_salida_f, double valor_f){//Inicio funcion conversion fuerza
	double resultado_f;
	return resultado_f = (unidad_salida_f/unidad_entrada_f)*valor_f;
}//Fin funcion conversion fuerza
double conversiones_densidad (double unidad_entrada_d, double unidad_salida_d, double valor_d){//Inicio funcion conversion densidad
	double resultado_d;
	return resultado_d = (unidad_salida_d/unidad_entrada_d)*valor_d;
}//Fin funcion conversion densidad
double conversiones_energia (double unidad_entrada_e, double unidad_salida_e, double valor_e){//Inicio funcion conversion energia
	double resultado_e;
	return resultado_e = (unidad_salida_e/unidad_entrada_e)*valor_e;
}//Fin funcion conversion energia
double conversiones_potencia (double unidad_entrada_i, double unidad_salida_i, double valor_i){
	double resultado_i;
	return resultado_i = (unidad_salida_i/unidad_entrada_i)*valor_i;
}
