/*
Autor: Fatima Azucena MC
Fecha: 05_01_2021
Correo: fatimaazucenamartinez274@gmail.com
*/

/*En un hospital existen 3 áreas: Urgencias, Pediatría y Traumatología.
El presupuesto anual del hospital se reparte de la siguiente manera: 
Pediatría 42% y Traumatología 21%.*/

#include <stdio.h>

int main(){//Inicio método preincipal
	//Declaracion de variables
	float presupuesto_Anual;
	float presupuesto_Pediatria;
	float presupuesto_Traumatologia;
	float presupuesto_Urgencias;
	float pediatria = 0.42;
	float traumatologia = 0.21;
	float urgencias = 0.37;

	printf("Ingrese el presupuesto anual: ");
	scanf("%f", &presupuesto_Anual);

	presupuesto_Traumatologia = presupuesto_Anual * traumatologia;
	presupuesto_Pediatria = presupuesto_Anual * pediatria;
	presupuesto_Urgencias = presupuesto_Anual * urgencias;

	printf("El presupuesto para el área de traumatologia es de: $ %f %s", presupuesto_Traumatologia, " pesos");
	printf("El presupuesto para el área de pediatria es de: $ %f %s", presupuesto_Pediatria, " peso");
	printf("El presupuesto para el área de urgencias es de: $ %f %s", presupuesto_Urgencias, " peso");
}//Fin método preincipal

