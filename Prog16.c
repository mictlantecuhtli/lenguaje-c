/*
 *Fecha: 12-08-2020
 *Autor: Fatima Azucena MC
 <fatimaazunemartinez274@gmail.com>*/

/*Realice un algoritmo que, con base en una calificación proporciona­da (0-10), indique con letra la calificación que le corresponde: 10 es “A”, 9 es “B”, 8 es “C”, 7 y 6 son “D”, y de 5 a 0 son “F”. Represente el diagrama de flujo, el pseudocódigo correspondiente.
*/

#include <stdio.h>
int main(){//Inicio metodo principal

        /*Define variables*/
	int opcion;

	printf ("Ingrese su calificacion: ");
	scanf ("%d" ,&opcion);
	switch (opcion){/*Inicio sentencia Switch-case*/
		case 10:
			printf("Sacaste A :D");
		break;
		case 9:
			printf("Sacaste B :D");
		break;
		case 8:
			printf("Sacaste C :)");
		break;
		case 7:
		case 6:
			printf("Sacaste D :)");
		break;
		case 5:
		case 4:
		case 3:
		case 2:
		case 1:
		case 0:
			printf ("Sacaste F :(");
		break;
		default:
			printf("Opcion no valida x");
		break;
       	}/*Fin sentencia Switch-case*/
}/*Fin metodo principal*/
