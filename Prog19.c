/*Fecha: 18-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com */

/*Realice un algoritmo para determinar qué cantidad de dinero hay en un monedero, considerando que se tienen monedas de diez, cinco y un peso, y billetes de diez, veinte y cincuenta pesos.*/


/*Libreria principal*/
#include <stdio.h>

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
        int x;
        int convertidor;
        double resultado;
        double monedero[ ] = { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, .50, .20, .10 };
        for(x=0;x<=12;x++){/*Inicio for 1*/
                printf("Ingrese la cantidad de $ %f ", monedero[x]," pesos que tiene: ");
                scanf("%d" , &convertidor);
                resultado=resultado+(convertidor*monedero[x]);

        }/*Fin for 1*/
        printf("El total de la caja registradora es de: %f $", resultado, " pesos");
}/*Fin metodo principal*/
