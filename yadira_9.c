/*
Fecha: 08_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartonez274@gmail.com
*/

/*Genera el factorial de un número.*/

#include <stdio.h>

int main(){//Inicio método principal
	//Declaracion de variables
	int numero, numero_Factorial = 1;

	printf("Digite un número: ");
	scanf("%d", &numero);

	for ( int i = 1; i <= numero; i++){//Inicio for_1
		numero_Factorial = numero_Factorial * i;
	}//Fin for_1
	printf("El factorial de %d %s %d", numero ," es: ", numero_Factorial);
}//Fin método principal
