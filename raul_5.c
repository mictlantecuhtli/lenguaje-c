/*
 *Fecha: 10_20_10
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com
 */

/*Calcular el salario actual de un empleado si obtuvo un
incremento del 8% sobre su salario actual y un descuento 
del 25% por sus servicios*/

#include <stdio.h>

int main(){//Inicio metodo principal
        //Declaracion de variables
        float salario = 0;
        char nombre[20];

        printf("Ingrese el nombre del empleado: ");
        scanf("%s", &nombre);
        printf("Ingrese su saldo actual: ");
        scanf("%f", &salario);

        salario = salario+(salario*0.08);
        salario = salario-(salario*0.25);

        printf("El saldo actual de %s %s %f %s", nombre ," es de: $", salario ," pesos \n");
}//Fin metodo principal
