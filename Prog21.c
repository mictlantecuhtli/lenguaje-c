/*
 *Fecha: 04_09_2020
 *Autor:Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com */

/**21Un vendedor ha realizado N ventas y desea saber cuántas fueron por 10,000 o menos, cuántas fueron por más de 10,000 pero por menos de 20,000, y cuánto fue el monto de las ventas de cada una y el monto global. Realice un algoritmo para determinar los totales. Represente la solución mediante diagrama de flujo, pseudocódigo.*/

/*Libreria principal*/
#include <stdio.h>
int main(){/*Inicio metodo principal*/
	/*Declaracion de variables*/
	int numVenta;
	float precioVenta;
	float montoGlobal=0;
	float montoMADE=0;
	float montoMEDE=0;
	int p;
	int v;

        /*Ingresa datos por teclados*/
	printf("Ingrese numero de ventas: ");
	scanf("%d", &numVenta);
	float g[numVenta];

	/*Inicio for 1*/
	for(p=0;p < numVenta;p++){
	   printf("Ingrese el precio de cada venta: ");
	   scanf("%f", &g[p]);
	   montoGlobal = montoGlobal + g[p];
	  
	}/*Fin for 1*/
	/*Inicio for 2*/
	for(v=0;v < precioVenta;v++){  
	   if(g[v]<=10000){
		   montoMEDE=montoMEDE+g[v];
	   }
	   else if ((g[v]>=10000) && (g[v]<200000)){
	   	   montoMADE=montoMADE+g[v];
	   }
	}/*Fin for 2*/
	printf("El monto global es de: %f %s %f %s %f", montoMADE,"+",montoMEDE," = ",montoGlobal);
	printf("\n");
}/*Fin metodo main*/
