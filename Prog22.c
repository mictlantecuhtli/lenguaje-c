/*
 *Fecha: 20-08-2020
 *Autor: Fatima Azucena MC 
 *fatimaazucenamartinez274@gmail.com*/

/*Se requiere determinar el costo que tendrá realizar una llamada te­lefónica con base en el tiempo que dura la llamada y en el costo por minuto.*/

/*Libreria principal*/
#include <stdio.h>

int main(){/*Inicio metodo principal*/
	/*Declaracion de variables*/
	int minuto;
	int precioMinuto;
	int resultado;

	printf("Ingrese el precio por minuto: ");
	scanf("%d", &precioMinuto);
	printf("Ingrese los minutos que tomo su llamada: ");
	scanf("%d", &minuto);

	resultado=(precioMinuto*minuto);

	printf("El costo que tendra su llamada es de: %d %s ", resultado, "pesos");
}/*Fin metodo principal*/    
