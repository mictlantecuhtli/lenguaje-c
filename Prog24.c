/*
 *Fecha: 25-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*La CONAGUA requiere determinar el pago que debe realizar una persona por el total de metros cúbicos que consume de agua.*/
 
/*Libreria prncipal*/
#include <stdio.h>

int main(){/*Inicio metodo principal*/
	/*Delaracion de variables*/
	int precioMetro;
	int metros;
	int resultado;

	printf("Ingrese el precio por metro cubico: ");
	scanf("%d", &precioMetro);
	printf("Ingrese los metros cubicos de agua consumida: ");
	scanf("%d", &metros);

	resultado=(precioMetro*metros);

	printf("El total a pagar es de: %d %s", resultado ," pesos");
}/*Fin metodo principal*/
