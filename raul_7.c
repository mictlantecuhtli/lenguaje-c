/*
 *Autor: Fatima Azucena MC
 *Fecha: 17_11_2020
 fatimaazucenamartinez274@gmail.com
 */

/*Escriba un algoritmo que dada la cantidad de monedas de 5-10-20 pesos, 
diga la cantidad de dinero que se tiene en total.*/

#include <stdio.h>

int main(){//Inicio metodo principal
	//Declaracion de variables
	 int i;
        int convertidor;
        double resultado;
        double cajero[ ] = { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, .50, .20, .10};
        for(i=0;i<=12;i++){/*Inicio for 1*/
                printf("Ingrese la cantidad de $ %f ", cajero[i]," pesos que tiene: ");
                scanf("%d" ,&convertidor);
                resultado=resultado+(convertidor*cajero[i]);

        }/*Fin for 1*/
        printf("El total de la caja registradora es de: %f %s$", resultado, " pesos");
}//Fin metodo principal

