/*
Alumna: Fatima Azucena MC
Fecha: 24_11_2020
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Calcular el nuevo salario de un empleado si se le descuenta el 20% de su salario actual.*/

//Libreria principal
#include <stdio.h>

int main(){//Inicio metodo principal
	//Declaracion de variables
	String nombre;
	double salario,nuevo_Salario;
	double DESCUENTO = 0.20;

	printf("Ingrese el nombre del empleado: ");
	scanf ("%s", &nombre);
	printf("Ingrese su salario actual: ");
	scanf("%f", &salario);

	nuevo_Salario = nuevo_Salario-(salario*DESCUENTO);

	printf("El sueldo de %s %s %f %s", nombre ," es de: ", nuevo_Salario ," pesos");
}//Fin metodo principal
