/*
Fecha: 26_12_2020
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Concatenar 3 numeros o letras y mostrar el resultado*/

#include <stdio.h>

int main(){//Inicio metodo principal
	//Declaracion de variables
	int valor_1;
	int valor_2;
	int valor_3;
	int valor_4;

	printf("Ingrese el primer numero o letra: ");
	scanf("%d", &valor_1);
	printf("Ingrese el segundo numero o letra: ");
        scanf("%d", &valor_2);
	printf("Ingrese el tercer numero o letra: ");
        scanf("%d", &valor_3);

	valor_4 = valor_1 + valor_2 + valor_3;

	printf("La concatenacion es: %d", valor_4);
}//Fin metodo principal
