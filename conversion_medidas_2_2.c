/*
Fecha: 05_04_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/******************************************/
//   Realizar un menu de conversiones    //
/*****************************************/

#include <stdio.h>
#define filas 4
#define columnas 10

int main(){//Inicio método principal
	String menu [filas][columnas] = {
		{"mm","cm","in","ft"},
		{"m^2","ft^2","mi^2","acre"},
		{"l","m^3","ft^3","galon americano"},
		{"kg","lb","gm","slug"},
		{"km/hr","mi/hr","ft/s","m/s"},
		{"N/m^2","lbf/in^2","atm","d/cm^2"},
		{"N","lbf","kgf","d"},
		{"gm/cm^3","lb/ft^3","kg/m^3","slug/ft^3"},
		{"w","j/s","erg/s","cal/h"},
		{"j","Nm","kcal","cal"}
	};
}//Fin método principal
