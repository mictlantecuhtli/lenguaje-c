/*
Fecha: 20_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*
Tema Estructuras cíclicas 
1_EN EL TECNOLÓGICO DE JILOTEPEC SE REALIZARÁ UNA ENCUESTA, PARA CONOCER 
MAYOR INFORMACIÓN DE LOS ALUMNOS Y VERIFICAR QUIENES DE ELLOS TIENEN O 
HAN TENIDO ALGUNA DIFICULTAD DE CONECTIVIDAD.  PARA LO CUAL LES ESTAN 
HACIENDO UN CUESTIONARIO CON LAS SIGUIENTES PREGUNTAS:
 • EDAD
 • LOCALIDAD
 • TIENEN INTERNET EN CASA
 • TIENEN COMPUTADORA EN CASA
 • CUENTA CON LOS RECURSOS BÁSICOS NECESARIOS DE ALIMENTACION.
2_RECOLECTA LOS DATOS Y PRESENTA LOS RESULTADOS CONSIDERANDO QUE SE 
DESCONOCE LA CANTIDAD DE ALUMNOS. 
3_IMPRIME LOS RESULTADOS CON NÚMEROS Y PORCENTAJES.
*/

#include <stdio.h>

int main(){//Inicio metodo principal
	//Declaracion de variables
	int localidad [100];
	int edad [100];
	int internet;
	int alimentacion;
	int computadora;
	int internet_si;
	int internet_no;
	int computadora_si;
	int computadora_no;
	int alimentacion_buena;
	int alimentacion_mala;
	double porcentaje;
	int conInternet = 0;
	int sinInternet = 0;
	int conComputadora = 0;
	int sinComputadora = 0;
	int buena_Alimentacion = 0;
	int mala_Alimentacion = 0;
	int cantidad_Alumnos;
	int canalejas = 0;
	int comunidad = 0;
	int san_Juan = 0;
	int continuar;
	int i;
	do{
		printf("Digite la cantidad de alumnos: ");
		scanf("%d", &cantidad_Alumnos);

		for ( i = 1; i <= cantidad_Alumnos; i++){//Inicio for_1
			printf("Digite la edad del alumno %d %s", i ,": ");
			scanf("%d", &edad[i]);
			printf("1)Canalejas\n2)San Juan Acazuchitlan\n3)Comunidad");
			printf("\nDigite su opcion: ");
			scanf("%d", &localidad[i]);
		

			switch (localidad[i]){//Inicio switch-case
				case 1:
					canalejas += 1;
				break;
				case 2:
					san_Juan += 1;
				break;
				case 3:
					comunidad += 1;
				break;
				default:
					printf("Opcion invalida, intente de nuevo");
				break;
			}//Fin switch-case
			printf("\n¿Cuenta con internet en casa?\n1)Si\n0)No");
			printf("\nDigite su opcion: ");
			scanf("%d", &internet);

			if ( internet == 1){//Inicio if_1
				conInternet += 1;
			}//Fin if_1
				else if ( internet == 0){//Inicio if_else_1
					sinInternet += 1;
				}//Fin if_else_1

			printf("¿Cuenta con computadora?:\n1)Si\n0)No ");
			printf("\nDigite su opcion: ");
			scanf("%d", &computadora);

			if ( computadora == 1){//Inicio if_2
				conComputadora += 1;
			}//Fin if_2
				else if ( computadora == 0){//Inicio if_else_2
					sinComputadora += 1;
				}//Fin if-else_2

			printf("¿Cuenta con una buena alimentacion?:\n1)Si\n0)No");
			printf("\nDigite su opcion: ");
			scanf("%d", &alimentacion);

			if ( alimentacion == 1){//Inicio if_3
				buena_Alimentacion += 1;
			}//Fin if_3

				else if ( alimentacion == 0){//Inicio if_else_3
					mala_Alimentacion += 1;
				}//Fin if_else_3
		}//Fin for_1
		porcentaje = (100/cantidad_Alumnos);
                internet_si = porcentaje*conInternet;
                internet_no = porcentaje*sinInternet;
                computadora_si = porcentaje*conComputadora;
                computadora_no = porcentaje*sinComputadora;
                alimentacion_mala = porcentaje*mala_Alimentacion;
                alimentacion_buena = porcentaje*buena_Alimentacion;

                printf("La encuesta se ha realizado a %d %s", cantidad_Alumnos, " alumnos");
                printf("\nDe esos alumnos %d %s %f %s", conInternet , " dijo que contaban con internet, lo que corresponde a un: ", internet_si ," porciento");
                printf("\n %d %s %f %s", sinInternet ," alumnos dijeron que no contaban con internet, lo que corresponde a un: ", internet_no ," porciento");
                printf("\n %d %s %f", conComputadora ," alumnos dijeron que contaban con computadora, lo que corresponde a un:  ", computadora_si );
		printf("%s", " porciento");
                printf("\n%d %s %f %s", sinComputadora ," alumnos dijeron que no contaban con computadora, lo que corresponde a un: ", computadora_si ," porciento");
		printf("\n %d %s %f", buena_Alimentacion , " alumnos dijeron que contaban con una buena alimentacion, lo que corresponde a un: ", alimentacion_buena);
		printf("%s", " porciento");
               	printf("\n %d %s %f", mala_Alimentacion , " alumnos dijeron que contaban con una mala alimentacion, lo que corresponde a un: ", alimentacion_mala);
		printf("%s", " porciento");
                printf("\n¿Que desea hacer?:\n1)Volver al menu\n0)Salir");
                printf("\nDigite su opcion: ");
                scanf("%d", &continuar);

	}while (continuar == 1);
}//Fin metodo principal
