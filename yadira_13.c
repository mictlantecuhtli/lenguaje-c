/*
Fecha: 25_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*
Solicitar la calificacion de las materias
las suma y saca el promedio.//adjunte una imagen en forma de tabla para que 
impresion de resultados se muestre de esa manera
*/

#include <stdio.h>

int main(){//Inicio metodo principal
	//Declaracion de variables
	typedef char String[20];
	char name [20];
	String materias [3];
	int calificacion_materia [3][5];
	char apellido_Paterno [20];
	char apellido_Materno [20];
	int grupo;
	char carrera [20];
	double suma_Promedios = 0;
	double promedio_General;
	double suma_Unidades;
	int promedio_materia;	

	printf("Digite su nombre: ");
	scanf("%s", &name);
	printf("Digite su apellido paterno: ");
	scanf("%s", &apellido_Paterno);
	printf("Digite su apellido materno: ");
	scanf("%s", &apellido_Materno);
	printf("Digite su grupo: ");
	scanf("%d", &grupo);
	printf("Digite su carrera: ");
	scanf("%s", &carrera);

	for (int i = 0; i < 3; i++){//Inicio for_1
		printf("\nDigite el nombre de la materia %d %s ", i + 1 ,": ");
		scanf("%s", &materias[i]);

		for (int j = 0; j < 5; j++){//Inicio for_2
			printf("\nDigite el promedio que obtuvo en la unidad %d %s", j + 1 ," (0-100): ");
			scanf("%d", &calificacion_materia[i][j]);
		}//Fin for_2
	}//Fin for_1

	
	for ( int a = 0; a < 3; a++){//Inicio for_3
		promedio_materia = 0;
		printf("\nAlumno: %s %s %s ", name , apellido_Paterno , apellido_Materno);
		printf("\nGrupo: %d", grupo);
		printf("\nCarrera: %s ", carrera);
		printf("\nMateria: %s ", materias[a]);
		printf("\n");
		printf("U1\tU2\tU3\tU4\tU5\n");
		for ( int b = 0; b < 5; b++){//Inicio for_4
			printf("%d %s", calificacion_materia[a][b] ,"\t");
			promedio_materia = promedio_materia + calificacion_materia[a][b];	
		}//Fin for_4
		suma_Unidades = promedio_materia / 5;
		suma_Promedios += suma_Unidades;
		printf("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-");
		printf("\n Promedio: %f ", suma_Unidades);
		printf("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-");
	}//Fin for_3
	promedio_General = suma_Promedios / 3;
	printf("\n\nEl promedio general es: %f ", promedio_General);
	printf("\n");
}//Fin metodo principal

