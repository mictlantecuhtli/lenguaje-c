/*Fecha: 02_10_20
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Calcula el precio de un boleto de viaje, tomando en cuenta 
el número de kilómetros que se van a recorrer. El precio por 
Kilometro es de $20.50*/

/*Libreria principal*/
#include <stdio.h>

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
        double costoBoleto;
        int kilometrosRecorridos;
        double PRECIOKILOMETRO=20.50;

        printf("Inserte los kilometros que va a recorrer: ");
        scanf("%d", &kilometrosRecorridos);

        costoBoleto=(kilometrosRecorridos*PRECIOKILOMETRO);

        printf("Usted a recorrido %d %s %f %s",kilometrosRecorridos," y el costo de su boleto es de: $",costoBoleto," pesos \n");
}/*Fin metodo principal*/
