/*Fecha: 17-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Se desea saber el total de una caja registradora de un almacén, se conoce el número de billetes y monedas, así como su valor. Realice un algoritmo para determinar el total. Represente la solución me­diante el diagrama de flujo, el pseudocódigo.
*/

/*Libreria principal*/
#include <stdio.h>

int main(){/*Inicio metodo principal*/
	/*Declaracion de variables*/
	int i;
	int convertidor;
	double resultado;
	double cajero[ ] = { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, .50, .20, .10};
	for(i=0;i<=12;i++){/*Inicio for 1*/
		printf("Ingrese la cantidad de $ %f ", cajero[i]," pesos que tiene: ");
		scanf("%d" ,&convertidor);
		resultado=resultado+(convertidor*cajero[i]);

	}/*Fin for 1*/
	printf("El total de la caja registradora es de: %f %s$", resultado, " pesos");
}/*Fin metodo principal*/
