/*
Fecha: 11_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmial.com*/

/*DESCRIPCIÓN : CALCULAR EL PROMEDIO DE LAS EDADES DEL GRUPO DE PRIMER SEMESTRE*/

#include <stdio.h>

int main(){//Inicio método principal
	//Declaracion de variables
	int cantidad_Alumnos;
	int edad;
	int suma_Edades [100];
	int resultado_Edad = 0;

	printf("¿Cuantos alumnos son?: ");
	scanf("%d", &cantidad_Alumnos);

	for ( int i = 1; i <= cantidad_Alumnos; i++){
		printf("Digite la edad del alumno %d %s", i,": ");
		scanf("%d", &suma_Edades[i]);
		resultado_Edad += suma_Edades[i];
	}
	resultado_Edad = resultado_Edad / cantidad_Alumnos;
	printf("El promedio de las edades es: %d %s", resultado_Edad,"%");
	printf("\n");
}//Fin método principal


