#include <stdio.h>

int main ()
{
    int op;
    float dato;
    printf("Conversiones\n");
    printf("1- Gramos a Libras\n");
    printf("2- Libras a Gramos\n");
    printf("3- Milimetros a Metros\n");
    printf("4- Metros a Milimetros\n");
    printf("5- Libras a Toneladas\n");
    printf("6- Toneladas a Libras\n");
    printf("7- Segundos a Dias\n");
    printf("8- Dias a Segundos\n");
    printf("Ingrese opcion: \n");
    scanf("%d",&op);
    
    switch (op)
    {
    case 1:
        printf("Ingrese cantidad de Gramos\n");
        scanf("%f",&dato);
        printf("Por %.2f Gramos hay %.2f Libras \n",dato,dato*0.0022046);
        break;
    case 2:
        printf("Ingrese cantidad de Libras\n");
        scanf("%f",&dato);
        printf("Por %.2f Libras hay %.2f Gramos\n",dato,dato/0.0022046);
        
        break;
    case 3:
        printf("Ingrese cantidad de Milimetros\n");
        scanf("%f",&dato);
        printf("Por %.2f Milimetros hay %.2f Metros\n",dato,dato/1000);
        break;
    case 4:
        printf("Ingrese cantidad de Metros\n");
        scanf("%f",&dato);
        printf("Por %.2f Metros hay %.2f Milimetros\n",dato,dato*1000);
        break;
    case 5:
        printf("Ingrese cantidad de Libras\n");
        scanf("%f",&dato);
        printf("Por %.2f Libras hay %.2f Toneladas\n",dato,dato/2204.6);
        break;
    case 6:
        printf("Ingrese cantidad de Toneladas\n");
        scanf("%f",&dato);
        printf("Por %.2f Toneladas hay %.2f Libras\n",dato,dato*2204.6);
        break;
    case 7:
        printf("Ingrese cantidad de Segundos\n");
        scanf("%f",&dato);
        printf("Por %.2f Segundos hay %.2f Dias\n",dato,dato*.0000115740);
        break;
    case 8:
        printf("Ingrese cantidad de Dias\n");
        scanf("%f",&dato);
        printf("Por %.2f Dias hay %.2f Segundos\n",dato,dato*24*60*60);
        break;
    
    }

    return 0;
}
