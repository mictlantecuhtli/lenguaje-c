/*
Fecha: 13_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/* EN EL TECNOLÓGICO NACIONAL DE MEXICO SE PUBLICÓ UNA
CONVOCATORIA DE ESTUDIOS EN EL EXTRANJERO, PODRÁN PARTICIPAR AQUELLOS
ESTUDIANTES QUE TENGAN UN PROMEDIO MAYOR A 90. LOS ALUMNOS QUE HAYAN
ALCANZADO LA CALIFICACIÓN ENTRARÁN DIRECTO A LA TÓMBOLA PARA EL SORTEO.
LOS ALUMNOS QUE NO ALCANCEN EL PROMEDIO PODRÁN PARTICIPAR EN LA SIGUIENTE
CONVOCATORIA SI APRUEBAN EL EXAMEN DEL IDIOMA PARA EL SIGUIENTE SEMESTRE
Y MEJORAN EN UN 20% SU PROMEDIO
*/

#include <stdio.h>

int main(){//Inicio método principal
	//Declaracion de variables
	int calificacion;
	int opcion;
	int examen_Idiomas;
	int continuar;
	int calificacion_Anterior;
		do {
			calificacion = 0;
			printf("B I E N V E N I D O S  A  L A  C O N V O C A T O R I A");
			printf("\n\t¿Vas a registrarte por primera vez?\n\t1)Si\n\t2)No");
			printf("\n\tDigite su opcion: ");
			scanf("%d", &opcion);

			switch (opcion){//Inicio switch-case
				case 1:
			       		printf("\n\tDigita tu calificacion (0-100): ");
					scanf("%d", &calificacion);

						if ( calificacion = 90 ){//Inicio if_else
							printf("\n\tHAZ ENTRADO DIRECTO A LA TOMBOLA, ¡FELICIDADES!");
						}
						else {
							printf("\n\tNO PUEDES ENTRAR A LA TOMBOLA, SUERTE PARA LA PRÓXIMA");
						}//Fin if_else	
				break;
				case 2:
					printf("\n\tTu ya haz participado anteriormente, el requisito ahora para que puedas entrar es: pasar el éxamen                                                            de idiomas y mejorar un 20 porciento tu calificacion anterior");
					printf("\n\t¿Pasaste el examen de idiomas?:\n\t1)Si\n\t2)No ");
					printf("\n\tDigite su opcion: ");
					scanf("%d", &examen_Idiomas);
					
						if ( examen_Idiomas == 1 ){
							printf("\n\t¿Cual fue tu calificacion anterior? (0-100): ");
							scanf("%d", &calificacion_Anterior);
							printf("\n\t¿Cual es tu calificacion actual? (0-100): ");
							scanf("%d", &calificacion);
							if ( calificacion > 90){
								printf("\n\tFELICIDADES,ENTRASTE A LA TOMBOLA, CUMPLES CON LOS 2 REQUISITOS");
							}
								else {
								printf("\n\tNO PUEDES ENTRAR A LA TOMBOLA, SUERTE PARA LA PROXIMA");
								}
						}
							else if( examen_Idiomas == 2){
								printf("\n\tNO PUEDES ENTRAR A LA TOMBOLA, YA QUE NO PASASTE UNO DE LOS REQUISITOS, SUERTE PARA LA PROXIMA");
							}
							
				break;
				default:
					printf("\n\tOpcion invalida, intente de nuevo");
				break;
		
			}//Fin switch-case
			printf("\n\t¿Que desea hacer?:\n\t1)Volver al menú\n\t0)Salir");
			printf("\n\tDigite su opcion: ");
			scanf("%d", &continuar);
		}while (continuar == 1);
	
}//Fin método principal
