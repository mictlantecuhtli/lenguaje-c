/*
Fecha: 28_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*
EN UNA TIENDA DEPARTAMENTAL ESTA PUBLICANDO NUEVAS OFERTAS POR
TEMPORADA NAVIDEÑA. SI LOS CLIENTES PRESENTAN UNA TARJETA DE CLIENTE FRECUENTE.
LES OTORGARÁN UN DESCUENTO DEL 20% DEL TOTAL DE SU COMPRA.
*/

#include <stdio.h>

int main(){//Inicio metodo principal
	//Declarar las variables
	int numero_Ventas;
	float venta_Descuento [100];
	float venta_no_Descuento [100];
	int opcion;
	int continuar;
	float suma_Venta = 0;

	do{//Inicio do-while
		printf("\nB I E N V E N I D O S\n\t¿Cuenta con una tarjeta de descuento?:\n\t1)Si\n\t2)No");
		printf("\nDigite su opcion: ");
		scanf("%d", &opcion);

		switch ( opcion ){//Inicio switch_case
			case 1:
				printf("\nDigite el numero de ventas: ");
				scanf("%d", &numero_Ventas);

				for ( int i = 0; i <= numero_Ventas; i++){
					printf("Digite el precio de la venta %d %s", i + 1 , ": ");
					scanf("%f", &venta_Descuento[i]);
					suma_Venta += venta_Descuento[i];
					suma_Venta = suma_Venta - (suma_Venta*0.20);
				}
				printf("El monto de su venta es de: %f %s", suma_Venta, " pesos");
			break;
			case 2:
				printf("Digite el numero de ventas: ");
				scanf("%d", &numero_Ventas);
			
				for ( int i = 0; i <= numero_Ventas; i++){
					printf("Digite el precio de la venta %d %s ", i + 1,": ");
					scanf("%f", &venta_no_Descuento[i]);
					suma_Venta += venta_no_Descuento[i];	
				}
				printf("El monto de la venta es de: %f %s", suma_Venta," pesos");
			break;
			default:
				printf("La opcion que ha digitado es incorrecta, intente de nuevo");
			break;
		}//Fin switch-case
		printf("\n¿Que desea hacer?\n1)Desea volver al menu\n0)Salir");
		printf("\nDigite su opcion: ");
		scanf("%d", &continuar);
	}while ( continuar == 1);
	printf("\nGracias por su preferencia, vuelva pronto");
}//Fin metodo principal

