/*
Fecha: 13_03_2021
Autor: Fatima MC
Correo: fatimaazucenamartinez274@gmail.com 
*/

/*Realizar un programa que convierta unidades de medida*/

#include <stdio.h>
int main(){//Inicio metodo principal
	//Declaracion de variables
	int opcion;
	int unidad;
	int conversion;
	double numero_km;
	double numero_m;
	double conversion_m;
	int conversion_m_i;
	int conversion_cm_i;
	int conversion_mm_i;
	double conversion_mm;
	double conversion_cm;
	int conversion_am_i;
	double conversion_am;
	double conversion_pulg;
	int continuar2;
	double conversion_m_km;
	double conversion_pie;
	double conversion_millas;
	int conversion_m_cm_i;
	double conversion_m_cm;
	int conversion_m_mm_i;
	double conversion_m_mm;
	double conversion_m_pul;
	double conversion_m_pie;
	double conversion_m_m;
	int continuar;

	do{
		printf("BIENVENIDOS AL MENU DE CONVERSIONES\n\t1)Longitud\n\t2)Área\n\t3)Volumen");
		printf("\n\t4)Masa\n\t5)Velocidad\n\t6)Densidad\n\t7)Fuerza\n\t8)Energía\n\t9)Potencia");
		printf("\n\t10)Presion");
		printf("\nDigite su opcion: ");
		scanf("%d", &opcion);

		switch (opcion){//Inicio switch-case
			case 1:
				printf("Usted a eleigo la opción de longitud, ¿Que unidad de medida tiene?: ");
				printf("\n\t1)Kilometro (Km)\n\t2)Metro (m)\n\t3)Centimetro (cm)\n\t4)milimetro (mm)");
				printf("\n\t5)Pulgada (pulg)\n\t6)Pie (pie)\n\t7)Milla (mi)");
				printf("\nDigite su ocpion: ");
				scanf("%d", &unidad);

				switch (unidad){//Inicio switch-case_2
					case 1:
						printf("A elegido la opción de Kilometro,¿A que medida quiere convertirlo?");
						printf("\n\t1)Metro (m)\n\t2)Centimetro (cm)\n\t3)Milimetro (mm)");
						printf("\n\t4)Pulgada (pulg)\n\t5)Pie (pie)\n\t6)Milla (mi)");
						printf("\nDigite su opcion: ");
						scanf("%d", &conversion);

						switch (conversion){//Inicio switch-case_3
							case 1:
								printf("A elegido convertir kilometros a metro");
								printf("\nDigite cuantos km quiere convertir: ");
								scanf("%lf", &numero_km);
								conversion_m = (numero_km*1000);
								conversion_m_i= (int)conversion_m;
								printf("Los %lf %s %d %s", numero_km, " kilometros equivale a ", conversion_m_i," metros");
							break;
							case 2:
								printf("A eligido convertir kilometros a centimetros");
								printf("\nDigite cuantos km quiere convertir: ");
								scanf("%lf", &numero_km);
								conversion_cm = (numero_km*1000*100);
								conversion_cm_i = (int)conversion_cm_i;
								printf("Los %lf %s %d %s", numero_km ," kilometros equivale a ", conversion_cm_i ," centimetros");
							break;
							case 3:
								printf("A eligido convertir kilometros a milimetros");
								printf("\nDigite cuantos km quiere convertir: ");
								scanf("%lf", &numero_km);
								conversion_mm = (numero_km*1000*1000);
								conversion_mm_i =(int)conversion_mm; 
								printf("Los %lf %s %d %s", numero_km," kilometos equivale a ", conversion_mm_i,"milimetos");
							break;	
							case 4:
								printf("A eligido convertir kilometros a pulgadas");
								printf("\nDigite los kilometros que desea converir: ");
								scanf("%lf", &numero_km);
								conversion_pulg = (numero_km*39370.08);
								printf("Los %lf %s %lf %s", numero_km, " kilometros equivale a ", conversion_pulg, " pulgadas");

							break;
							case 5:
								printf("A elegido convertir kilometros a pies");
								printf("\nDigite cuantos kilometros desea convertir: ");
								scanf("%lf", &numero_km);
								conversion_pie = (numero_km*3280.84);
								printf("Los %lf %s %lf %s", numero_km, " kilometros equivale a ", conversion_pie, " pies");
							break;
							case 6:
								printf("A eligido convertir kilometros a millas");
								printf("\nDigite cuantos kilometros desea convertir: ");
								scanf("%lf", &numero_km);
								conversion_millas = (numero_km*0.6214);
								printf("Los %lf %s %lf %s", numero_km, " kilometros equivale a ", conversion_millas," millas");
							break;
						}//Fin switch-case_3
					break;
					case 2:
						printf("A elegido la opcion metro, ¿A que unidad desea convertirlo?");
						printf("\n\t1)Kilometro (km)\n\t2)Centimetro (cm)\n\t3)Milimetros (mm)");
						printf("\n\t4)Pulgadas (pulg)\n\t5)Pie (pie)\n\t6)Millas (mi)");
						printf("\nDigite su opcion: ");
						scanf("%d", &conversion);

						switch (conversion){
							case 1:
								printf("A elegido convertir metros a kilometros");
								printf("\nDigite cuantos metros desea convertir: ");
								scanf("%lf", &numero_m);
								conversion_m_km = (numero_m* 0.001);
								printf("Los %lf %s %lf %s", numero_m, " metros equivale a ", conversion_m_km, " kilometros");
							break;
							case 2:
								printf("A elegido convertir metros a centimetros");
                                                                printf("\nDigite cuantos metros desea convertir: ");
                                                                scanf("%lf", &numero_m);
                                                                conversion_m_cm = (numero_m* 100);
								conversion_m_cm_i = (int)conversion_m_cm;
                                                                printf("Los %lf %s %d %s", numero_m, " metros equivale a ", conversion_m_cm_i, " centimetros");
							break;
							case 3:
								printf("A elegido convertir metros a milimetros");
								printf("\nDigite cuantos metros desea convertir: ");
								scanf("%lf", &numero_m);
								conversion_m_mm = (numero_m*1000);
								conversion_m_mm_i = (int)conversion_m_mm;
								printf("Los %lf %s %d %s", numero_m, " metros equivale a ", conversion_m_mm_i, " milimetros");
							break;
							case 4:
								printf("A elegido convertir metros a pulgadas");
								printf("\nDigite cuantos metros desea convertir: ");
								scanf("%lf", &numero_m);
								conversion_m_pul = (numero_m*39.3701);
								printf("Los %lf %s %lf %s", numero_m," metros equivale a ", conversion_m_pul," pulgadas");
							break;
							case 5:
								printf("A elegido convertir metros a pies");
								printf("\nDigite cuantos metros desea convertir: ");
								scanf("%lf", &numero_m);
								conversion_m_pie = (numero_m* 3.2808);
								printf("Los %lf %s %lf %s", numero_m," metros equivale a ", conversion_m_pie," pies");
							break;
							case 6:
								printf("A elegido convertir metros a millas");
								printf("\nDigite cuantos metros desea convertir: ");
								scanf("%lf", &numero_m);
								conversion_m_m = (numero_m*0.000621);
								printf("Los %lf %s %lf", numero_m," metros equivale a ", conversion_m_m);
							break;
						
					break;
					case 3:
						printf("A elegido la opcion de centimetros, ¿A que unidad deseas convertir:");
						printf("\n\t1)Kilometros (km)\n\t2)Metro (m)\n\t3)Milimetro (mm)");
						printf("\n\t4)Pulgada\n\t5)Pie (pie)\n\t6)Milla (mi)");
						printf("\n\tDigite su opcion: ");
							
					break;
					case 4:
	
					break;
					case 5:

					break;
					case 6:
				
					break;
					case 7:
	
					break;
					case 8:
	
					break;	
					case 9:

					break;
				}//Fin switch-case_2
			break;
			case 2:

			break;
			case 3:

			break;
			case 4:

			break;
			case 5:

			break;
			case 6:

			break;
			case 7:

			break;
			case 8:

			break;
			case 9:

			break;
		}//Fin switch case
		printf("\n¿Que desea hacer?\n1)Volver\n0)Salir");
		printf("\nDigite su opcion: ");
		scanf("%d" , &continuar);
	}while (continuar == 1);
}//Fin método principal
