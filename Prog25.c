/*
Fecha: 27_11_2020
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*25-Realice un algoritmo que determine el sueldo semanal de N trabajadores 
considerando que se les descuenta 5% de su sueldo si ganan entre 0 y 150 pesos.
Se les descuenta 7% si ganan más de 150 pero menos de 300, y 9% si ganan más de 
300 pero menos de 450. Los datos son horas trabajadas, sueldo por hora y nombre 
de cada trabajador. Represéntelo mediante diagrama de flujo, pseudocódigo.
*/

//Libreria principal
#include <stdio.h>

int main(){//Inicio metodo principal
	//Declaracion de variables
	float sueldoHoras;
	int canTrabajadores;
	String nombre;
	int hTrabajadas;
	float dineroGanado;
	float sueldoBruto;
	float sueldoNeto;

	//Entrada de datos por teclado
	printf("Inserte el nombre del trabajador: ");
	scanf("%s",&nombre);
	printf("Inserte las horas trabajadas: ");
	scanf("%d", &hTrabajadas);
	printf("Inserte el dinero ganado: ");
	scanf("%f", &dineroGanado);

	if ((dineroGanado>0)&&(dineroGanado<=150)){
		sueldoBruto=40*(dineroGanado/hTrabajadas);
		sueldoNeto=sueldoNeto-(sueldoNeto*0.05);
	}
	 else if ((dineroGanado>=150)&&(dineroGanado<=300)){
	 	sueldoBruto=40*(dineroGanado/hTrabajadas);
                sueldoNeto=sueldoNeto-(sueldoNeto*0.07);
	 }
	  else if ((dineroGanado>=350)&&(dineroGanado<=450)){
	  	sueldoBruto=40*(dineroGanado/hTrabajadas);
                sueldoNeto=sueldoNeto-(sueldoNeto*0.09);
	  }

	  printf("El sueldo bruto es de %s %f %s", nombre, " es de: ", sueldoBruto ," pesos");
	  printf("El sueldo bruto es de %s %f %s", nombre, " es de: ", sueldoNeto ," pesos");
}//Fin metodo principal
