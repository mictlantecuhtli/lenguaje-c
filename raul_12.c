/*
Fecha: 15_12_2020
Autor: Fatima Azucena Martínez Cadena
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Un vendedor recibe un sueldo base más un 10% por comision de sus ventas
el vendedor desea saber cuanto dinero obtendra por concepto de comisiones
por las 3 ventas que realiza en el mes y el total que recibira en el mes
tomando en cunta su base y comisiones*/

#include <stdio.h>

int main(){//Inicio metodo principal
        //Declaracion de variables
        float comision = 0;
        int cambio = 0;
        float sueldoFijo = 0;
        int numVentas;
        float precioVenta = 0;
        float sueldoTotal = 0;

        printf("Ingrese su sueldo base:");
        scanf("%f", &sueldoFijo);
        printf("Ingrese el nuero de ventas que realizo");
        scanf("%d", &numVentas);

        for(int i = 0; i <= numVentas; i++){//Inicio for_1
                cambio = cambio + 1;
                printf("Ingrese el precio de la venta ", &cambio , ": ");
                scanf("%f", &precioVenta);
                comision = comision + (precioVenta*0.10);
        }//Fin for_!
        sueldoTotal = comision + sueldoFijo;
        printf("El total de tus comisiones es de: %f %s", comision ,  " pesos");
        printf("Tu sueldo total es de: %f %s", sueldoTotal , " pesos");
}//Fin metodo principal

