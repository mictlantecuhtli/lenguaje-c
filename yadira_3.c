/*
Fecha: 06_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Un programa solicita que sean capturados 3 datos númericos y
a partir de ellos imprimirá si es par*/

//Libreria principal
#include <stdio.h>

int main(){//Inicio metodo principal
        //Declaracion de variables
        int numero_1;
        int numero_2;
        int numero_3;

        printf("Digite el primer número: ");
        scanf("%d", &numero_1);
        printf("Digite el segundo número: ");
        scanf("%d", &numero_2);
        printf("Digite el tercer número: ");
	 scanf("%d", &numero_3);

        if ( numero_1%2==0){
                printf("\nEl número ", numero_1, " es par");
        }
        else {
                printf("\nEl número", numero_1, " es impar");
        }

         if ( numero_2%2==0){
                printf("\nEl número ", numero_2, " es par");
        }
        else {
                printf("\nEl número", numero_2, " es impar");
        }

         if ( numero_3%2==0){
                printf("\nEl número ", numero_3, " es par");
        }
        else {
                printf("\nEl número", numero_3, " es impar");
	 }


}//Fin metodo principal


