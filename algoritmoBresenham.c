/*
Fecha: 16_06_2021
Autor: Fátima Azucena MC
Correo: fatimaazucenamartínez274@gmail.com
*/

#include <stdio.h>

int algoritmoBresenham(int x1, int y1, int x2, int y2);

int main(){//Inicio metodo principal
	//Declaracion de variables
	int a1;
	int b1;
	int a2;
	int b2;

	printf("Digite el valor de la coordenada x1: ");
	scanf("%d", &a1);
	printf("Digite el valor de la coordenada y1: ");
        scanf("%d", &b1);
	printf("Digite el valor de la coordenada x2: ");
        scanf("%d", &a2);
	printf("Digite el valor de la coordenada y2: ");
        scanf("%d", &b2);

	algoritmoBresenham(a1,b1,a2,b2);
}//Fin metodo principal
int algoritmoBresenham(int x1, int y1, int x2, int y2){
	int x, y, dx, dy, p;
	x = x1;
	y = y1;
	dx = x2 - x1;
	dy = y2 - y1;
	p = 2*dy - dx;

	while (x <= x2){
		printf("%s %d %s %d %s", "x: ", x," y: ", y, "\n");
		x++;
		if ( p < 0){
			p = p + 2 * dy;
		}
		else{
			p = p + ( 2 * dy) - (2 * dx);
			y++;
		}
	}//Fin while_1;
}//Fin función algoritmoBresenham
